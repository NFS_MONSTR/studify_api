from studify_api.studify_api import *

api = StudifyApi()

api.connect()

if api.login('o12908829@nwytg.net', 'qwerty'):

    pair1 = Pair(
        group=api.user.group.id,
        name='Пара через новое',
        type='Лекция',
        time=PairTime(start='8:30',end='9:05'),
        period=PairPeriod(start='04.02.2019', end='09.05.2019',weekday=3, week=0),
        audiences=['Г-111'],
        teachers=['Препод'],
    )
    pair2 = Pair(
        group=api.user.group.id,
        name='Пара через новое по дате',
        type='Лекция',
        time=PairTime(start='8:30', end='9:05'),
        date=['01.03.2019'],
        audiences=['Г-111'],
        teachers=['Препод'],
    )
    #api.add_pairs([pair1,pair2])
    #print(api.get_schedule()['lessons'])
    #api.skip_pairs([PairSkip(pair_id='5c7033287acf4770c1e65160',skip_date='')])
    #api.delete_pairs(['5c7033287acf4770c1e65160'])
    #print(api.get_schedule()['lessons'])
    pairs = api.get_pairs()
    print(pairs)

api.logout()

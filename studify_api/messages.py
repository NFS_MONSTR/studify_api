import ujson

from studify_api.util import generate_uuid


def for_name(name, *args):
    return globals()[name](*args)


class BaseMessage:
    def __init__(self, api=None, uid=None, cuid='', data=None):
        self.api = api
        if not uid:
            self._uid = str(generate_uuid())
        else:
            self._uid = uid
        self._cuid = cuid
        if not data:
            data = {}
        self._data = ujson.dumps(data)

    def set_cuid(self, cuid):
        self._cuid = cuid

    def before_send(self):
        pass

    def send(self):
        pass

    def on_receive(self):
        if (self._uid != None):
            message = Success()
            message.set_cuid(self._uid)
            self.api.send_message(message)

    def get_json(self):
        return self._data

    def get_uid(self):
        return self._uid

    def get_cuid(self):
        return self._cuid

    def get_name(self):
        return self.__class__.__name__.replace("Message","")


class Success(BaseMessage):
    '''
    Приходит, если команда выполнена успешно
    Нужно отвечать им на любое сообщение от сервера, кроме него самого
    Если не ответить - сервер будет при следующем запросе повторно отсылать уже полученное
    '''
    def on_receive(self):
        pass


#todo UNUSED
class GetCitys(BaseMessage):
    '''
    Запрос на получение списка городов
    В ответ приходит GetCitysResult
    '''
    pass


class Logout(BaseMessage):
    '''
    Выйти из аккаунта и отключиться от api(token умирает)
    '''
    pass


class SignIn(BaseMessage):
    '''
    Вход в аккаунт(на самом деле есть несколько команд)
    '''
    pass


class Register(BaseMessage):
    '''
    Получить свежий api токен
    '''
    pass


#todo UNUSED
class UpdateClient(BaseMessage):
    '''
    Обновить api токен
    '''
    pass


class GroupScheduleRequest(BaseMessage):
    '''
    Получение информации о группе и расписании
    '''
    pass


#todo Not working
class SetGroupScheduleRequest(BaseMessage):
    '''
    По-идее должен устанавливать расписание целиком, используется
    онлайн-редактором
    '''
    pass


class EditorGroupScheduleRequest(BaseMessage):
    '''
    Используется онлайн-редактором, возвращает расписание
    '''
    pass


class DeleteLesson(BaseMessage):
    '''
    Удаляет пару с указанным id
    '''
    pass


class SetLesson(BaseMessage):
    '''
    Добавляет или изменяет(если id указан) пару
    '''
    pass


class SkipLesson(BaseMessage):
    '''
    Отменяет пару на указанную дату(чтобы отменить отмену нужно отправить без даты)
    Для каждой пары хранится только последняя отмена
    '''
    pass




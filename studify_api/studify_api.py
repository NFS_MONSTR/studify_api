from collections import namedtuple

import requests
from studify_api.messages import *

from studify_api.util import get_current_time

CONFIG_BASE_URL = 'http://api.rvuzov.ru/v2/mq/'
CONFIG_DEVICE = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.81 Safari/537.36'
CONFIG_OS = 'Linux x86_64'
CONFIG_V = '1.0.0'


class StudifyApi():

    def __init__(self):
        self.config = {
            'url': CONFIG_BASE_URL,
            'device': CONFIG_DEVICE,
            'os': CONFIG_OS,
            'v': CONFIG_V,
        }
        self.token = ''
        self.user = None

    def connect(self):
        '''
        Подключается к api studify, получает токен
        :return: Nothing
        '''
        data = {
            'device': self.config['device'],
            'os': self.config['os'],
            'v' :self.config['v'],
            'lang': 'ru',
            'app': 'student'
        }
        status, result = self.execute_command(Register(data=data), 'RegisterResult')
        #todo move to exception
        assert status
        self.token = result['token']

    def auth_required(method):
        def wrapper(self, *args, **kwargs):
            assert self.user != None
            return method(self, *args, **kwargs)

        return wrapper

    def send_message(self, message):
        data = {
            'type': message.get_name(),
            'uid': message.get_uid(),
            'cuid': message.get_cuid(),
            'data': message.get_json(),
            'time': get_current_time()
        }
        data = ujson.dumps([data])
        if message.__class__.__name__ != 'Register':
            post_fields = {'token': self.token, 'data': data}
        else:
            post_fields = {'data': data}

        response = requests.post(self.config['url'], data=post_fields)

        success = False

        if response.status_code == 200:
            messages = ujson.loads(response.text)
            for x in messages:
                try:
                    msg = for_name(x['type'], self, x['uid'], x['cuid'], x['data'])
                except KeyError:
                    msg = BaseMessage(self, x['uid'], x['cuid'], x['data'])
                if (x['type'] == 'Success') and (x['cuid'] == message.get_uid()):
                    success = True
                msg.on_receive()
            return success, messages
        else:
            print(response.text)  # TEXT/HTML
            print(response.status_code, response.reason)  # HTTP
            return success, []

    def send_messages(self, messages):
        p_data = []
        for message in messages:
            data = {
                'type': message.__class__.__name__,
                'uid': message.get_uid(),
                'cuid': message.get_cuid(),
                'data': message.get_json(),
                'time': get_current_time()
            }
            p_data.append(data)

        post_fields = {'token': self.token, 'data': ujson.dumps(p_data)}

        response = requests.post(self.config['url'], data=post_fields)

        messages_uid = [message.get_uid() for message in messages]

        if response.status_code == 200:
            messages = ujson.loads(response.text)
            for x in messages:
                try:
                    msg = for_name(x['type'], self, x['uid'], x['cuid'], x['data'])
                except KeyError:
                    msg = BaseMessage(self, x['uid'], x['cuid'], x['data'])
                if (x['type'] == 'Success') and (x['cuid'] in messages_uid):
                    messages_uid.remove(x['cuid'])
                msg.on_receive()
            return len(messages_uid)==0, messages
        else:
            print(response.text)  # TEXT/HTML
            print(response.status_code, response.reason)  # HTTP
            return len(messages_uid)==0, []

    @staticmethod
    def get_message(messages, name):
        return [x for x in messages if x['type'] == name][0]

    @staticmethod
    def get_message_data(messages, name):
        return ujson.loads(StudifyApi.get_message(messages, name)['data'])

    def execute_command(self, message, result_name):
        status, result = self.send_message(message)
        if (status):
            return status, self.get_message_data(result, result_name)
        return None

    def login(self, email, password):
        '''
        Авторизуется в api от имени пользователя
        :param email: email пользователя
        :param password: password - пароль
        :return: True - если авторизация успешна, False если нет. В StudifyApi.user - появляются сведения о пользователе
        '''
        status, messages = self.send_message(SignIn(data={'email': email, 'password': password}))
        if status:
            profileResultData = self.get_message_data(messages, 'ProfileResult')
            id = profileResultData['id']
            name = profileResultData['name']
            email = profileResultData['email']
            groupData = self.get_message_data(messages, 'GroupChatStart')
            group = Group(groupData['group'], groupData['name'])
            self.user = User(id, name, email, group)
        return status

    def logout(self):
        '''
        Отключается от api studify и выходит из аккаунта
        :return: Nothing
        '''
        if self.send_message(Logout())[0]:
            self.token = ''
            self.user = None
            return True
        return False

    @auth_required
    def get_schedule(self):
        '''
        Получение расписания
        :return: Словарь с расписанием вида:
        {'university': {'id': '59e404097acf4702e3f2dfda', 'name': 'тест', 'abbr': 'тест'}, 'faculty': {'id': '59e404097acf4702e3f2dfdb', 'name': 'тест'}, 'group': {'id': '5c6c5a847acf4770c1810987', 'name': 'тест'}, 'name': 'тест', 'lessons': '[{"id":"5c7032107acf4770c1e5bf02","subject":"Пара через новое по дате","type":"Лекция","time":{"start":"8:30","end":"9:05"},"date":"01.03.2019","period":{},"disciplines":[],"audiences":[{"name":"Г-111"}],"teachers":[{"name":"Препод","email":""}],"subgroups":"","note":"","group":{"id":"5c6c5a847acf4770c1810987","name":"тест"},"university":{"id":"59e404097acf4702e3f2dfda","name":"тест","abbr":"тест"},"faculty":{"id":"59e404097acf4702e3f2dfdb","name":"тест"},"skip":"","official":false,"private":false,"user":"5c6c5a137acf4770c1807c39"},{"id":"5c7032107acf4770c1e5bf00","subject":"Пара через новое","type":"Лекция","time":{"start":"8:30","end":"9:05"},"date":"06.02.2019,13.02.2019,20.02.2019,27.02.2019,06.03.2019,13.03.2019,20.03.2019,27.03.2019,03.04.2019,10.04.2019,17.04.2019,24.04.2019,01.05.2019,08.05.2019","period":{"start":"04.02.2019","end":"09.05.2019","weekday":3},"disciplines":[],"audiences":[{"name":"Г-111"}],"teachers":[{"name":"Препод","email":""}],"subgroups":"","note":"","group":{"id":"5c6c5a847acf4770c1810987","name":"тест"},"university":{"id":"59e404097acf4702e3f2dfda","name":"тест","abbr":"тест"},"faculty":{"id":"59e404097acf4702e3f2dfdb","name":"тест"},"skip":"","official":false,"private":false,"user":"5c6c5a137acf4770c1807c39"},{"id":"5c7031f57acf4770c1e5befd","subject":"Пара через новое по дате","type":"Лекция","time":{"start":"8:30","end":"9:05"},"date":"01.03.2019","period":{},"disciplines":[],"audiences":[{"name":"Г-111"}],"teachers":[{"name":"Препод","email":""}],"subgroups":"","note":"","group":{"id":"5c6c5a847acf4770c1810987","name":"тест"},"university":{"id":"59e404097acf4702e3f2dfda","name":"тест","abbr":"тест"},"faculty":{"id":"59e404097acf4702e3f2dfdb","name":"тест"},"skip":"","official":false,"private":false,"user":"5c6c5a137acf4770c1807c39"},{"id":"5c7031f57acf4770c1e5befb","subject":"Пара через новое","type":"Лекция","time":{"start":"8:30","end":"9:05"},"date":"06.02.2019,13.02.2019,20.02.2019,27.02.2019,06.03.2019,13.03.2019,20.03.2019,27.03.2019,03.04.2019,10.04.2019,17.04.2019,24.04.2019,01.05.2019,08.05.2019","period":{"start":"04.02.2019","end":"09.05.2019","weekday":3},"disciplines":[],"audiences":[{"name":"Г-111"}],"teachers":[{"name":"Препод","email":""}],"subgroups":"","note":"","group":{"id":"5c6c5a847acf4770c1810987","name":"тест"},"university":{"id":"59e404097acf4702e3f2dfda","name":"тест","abbr":"тест"},"faculty":{"id":"59e404097acf4702e3f2dfdb","name":"тест"},"skip":"","official":false,"private":false,"user":"5c6c5a137acf4770c1807c39"},{"id":"5c7031db7acf4770c1e5bef8","subject":"Пара через новое по дате","type":"Лекция","time":{"start":"8:30","end":"9:05"},"date":"01.03.2019","period":{},"disciplines":[],"audiences":[{"name":"Г-111"}],"teachers":[{"name":"Препод","email":""}],"subgroups":"","note":"","group":{"id":"5c6c5a847acf4770c1810987","name":"тест"},"university":{"id":"59e404097acf4702e3f2dfda","name":"тест","abbr":"тест"},"faculty":{"id":"59e404097acf4702e3f2dfdb","name":"тест"},"skip":"","official":false,"private":false,"user":"5c6c5a137acf4770c1807c39"},{"id":"5c7031db7acf4770c1e5bef6","subject":"Пара через новое","type":"Лекция","time":{"start":"8:30","end":"9:05"},"date":"06.02.2019,13.02.2019,20.02.2019,27.02.2019,06.03.2019,13.03.2019,20.03.2019,27.03.2019,03.04.2019,10.04.2019,17.04.2019,24.04.2019,01.05.2019,08.05.2019","period":{"start":"04.02.2019","end":"09.05.2019","weekday":3},"disciplines":[],"audiences":[{"name":"Г-111"}],"teachers":[{"name":"Препод","email":""}],"subgroups":"","note":"","group":{"id":"5c6c5a847acf4770c1810987","name":"тест"},"university":{"id":"59e404097acf4702e3f2dfda","name":"тест","abbr":"тест"},"faculty":{"id":"59e404097acf4702e3f2dfdb","name":"тест"},"skip":"","official":false,"private":false,"user":"5c6c5a137acf4770c1807c39"},{"id":"5c6ecd737acf4770c1c00c2d","subject":"Новая пара","type":"Лекция","time":{"start":"08:00","end":"09:30"},"date":"02.02.2019,09.02.2019,16.02.2019,23.02.2019,02.03.2019,09.03.2019,16.03.2019,23.03.2019,30.03.2019,06.04.2019,13.04.2019,20.04.2019,27.04.2019,04.05.2019,11.05.2019,18.05.2019,25.05.2019","period":{"start":"01.02.2019","end":"31.05.2019","weekday":6},"disciplines":[],"audiences":[{"name":"вв"}],"teachers":[{"name":"авыавы","email":""}],"subgroups":"вв","note":"","group":{"id":"5c6c5a847acf4770c1810987","name":"тест"},"university":{"id":"59e404097acf4702e3f2dfda","name":"тест","abbr":"тест"},"faculty":{"id":"59e404097acf4702e3f2dfdb","name":"тест"},"skip":"23.03.2019","official":false,"private":false,"user":"5c6c5a137acf4770c1807c39"},{"id":"5c6ecda37acf4770c1c00c35","subject":"Новая пара","type":"Лекция","time":{"start":"08:00","end":"09:30"},"date":"12.02.2019,26.02.2019,12.03.2019,26.03.2019,09.04.2019,23.04.2019,07.05.2019,21.05.2019","period":{"start":"01.02.2019","end":"31.05.2019","weekday":2,"week":1},"disciplines":[],"audiences":[{"name":"вв"}],"teachers":[{"name":"авыавы","email":""}],"subgroups":"вв","note":"","group":{"id":"5c6c5a847acf4770c1810987","name":"тест"},"university":{"id":"59e404097acf4702e3f2dfda","name":"тест","abbr":"тест"},"faculty":{"id":"59e404097acf4702e3f2dfdb","name":"тест"},"skip":"","official":false,"private":false,"user":"5c6c5a137acf4770c1807c39"},{"id":"5c6ecdc37acf4770c1c00c38","subject":"Новая пара чёт","type":"Лекция","time":{"start":"08:00","end":"09:30"},"date":"05.02.2019,19.02.2019,05.03.2019,19.03.2019,02.04.2019,16.04.2019,30.04.2019,14.05.2019,28.05.2019","period":{"start":"01.02.2019","end":"31.05.2019","weekday":2,"week":2},"disciplines":[],"audiences":[{"name":"вв"}],"teachers":[{"name":"авыавы","email":""}],"subgroups":"вв","note":"","group":{"id":"5c6c5a847acf4770c1810987","name":"тест"},"university":{"id":"59e404097acf4702e3f2dfda","name":"тест","abbr":"тест"},"faculty":{"id":"59e404097acf4702e3f2dfdb","name":"тест"},"skip":"","official":false,"private":false,"user":"5c6c5a137acf4770c1807c39"},{"id":"5c6ec47f7acf4770c1bee85f","subject":"Ещё предмет","type":"Практика","time":{"start":"05:00","end":"22:00"},"date":"07.02.2019,14.02.2019,21.02.2019,28.02.2019,07.03.2019,14.03.2019,21.03.2019,28.03.2019,04.04.2019,11.04.2019,18.04.2019,25.04.2019,02.05.2019,09.05.2019,16.05.2019,23.05.2019,30.05.2019","period":{"start":"01.02.2019","end":"31.05.2019","weekday":4},"disciplines":[],"audiences":[{"name":"Новая аудитория"}],"teachers":[{"name":"Другой преподаватель","email":""}],"subgroups":"","note":"","group":{"id":"5c6c5a847acf4770c1810987","name":"тест"},"university":{"id":"59e404097acf4702e3f2dfda","name":"тест","abbr":"тест"},"faculty":{"id":"59e404097acf4702e3f2dfdb","name":"тест"},"skip":"","official":false,"private":false,"user":""},{"id":"5c6ec47f7acf4770c1bee85e","subject":"тест","type":"Практика","time":{"start":"05:00","end":"15:05"},"date":"12.02.2019","period":{},"disciplines":[],"audiences":[{"name":"Аудитория-2"}],"teachers":[{"name":"Другой преподаватель","email":""}],"subgroups":"","note":"","group":{"id":"5c6c5a847acf4770c1810987","name":"тест"},"university":{"id":"59e404097acf4702e3f2dfda","name":"тест","abbr":"тест"},"faculty":{"id":"59e404097acf4702e3f2dfdb","name":"тест"},"skip":"","official":false,"private":false,"user":""},{"id":"5c6ec47f7acf4770c1bee85d","subject":"Пара_share_with_group","type":"Лекция","time":{"start":"08:00","end":"09:30"},"date":"08.02.2019,22.02.2019,08.03.2019,22.03.2019,05.04.2019,19.04.2019,03.05.2019,17.05.2019,31.05.2019","period":{"start":"01.02.2019","end":"31.05.2019","weekday":5,"week":2},"disciplines":[],"audiences":[{"name":"11"}],"teachers":[{"name":"Препод","email":""}],"subgroups":"","note":"","group":{"id":"5c6c5a847acf4770c1810987","name":"тест"},"university":{"id":"59e404097acf4702e3f2dfda","name":"тест","abbr":"тест"},"faculty":{"id":"59e404097acf4702e3f2dfdb","name":"тест"},"skip":"21.02.2019","official":false,"private":false,"user":""},{"id":"5c6fb96a7acf4770c1d7ca2b","subject":"By dates","type":"Лекция","time":{"start":"08:00","end":"09:30"},"date":"13.02.2019,20.02.2019,28.02.2019,14.03.2019","period":{},"disciplines":[],"audiences":[{"name":"aa"}],"teachers":[{"name":"te","email":""}],"subgroups":"","note":"","group":{"id":"5c6c5a847acf4770c1810987","name":"тест"},"university":{"id":"59e404097acf4702e3f2dfda","name":"тест","abbr":"тест"},"faculty":{"id":"59e404097acf4702e3f2dfdb","name":"тест"},"skip":"","official":false,"private":false,"user":"5c6c5a137acf4770c1807c39"},{"id":"5c6db91d7acf4770c1a2d1b9","subject":"Приватная пара111","type":"Лекция","time":{"start":"08:00","end":"09:30"},"date":"10.02.2019,24.02.2019,10.03.2019,24.03.2019,07.04.2019,21.04.2019,05.05.2019,19.05.2019","period":{"start":"01.02.2019","end":"31.05.2019","weekday":7,"week":2},"disciplines":[],"audiences":[{"name":"22"}],"teachers":[{"name":"1232132","email":""}],"subgroups":"1","note":"","group":{"id":"","name":""},"university":{"id":"","name":"","abbr":""},"faculty":{"id":"","name":""},"skip":"23.02.2019","official":false,"private":true,"user":"5c6c5a137acf4770c1807c39"}]', 'elder': {'id': '', 'name': '', 'avatar': '', 'access': '', 'url': ''}, 'parity': False, 'isnew': False}
        '''
        return self.execute_command(GroupScheduleRequest(data={'group_id': self.user.group.id}), 'GroupScheduleResponse')[1]


    @auth_required
    def get_pairs(self):
        '''
        Получение списка пар
        :return: Список пар
        '''
        #(self, id='', group='', name='', type='', time=None, date=None, period=None, audiences=None, teachers=None, note='', subgroups='', private=False, skip=''):
        pairs = []
        lessons = ujson.loads(self.execute_command(GroupScheduleRequest(data={'group_id': self.user.group.id}), 'GroupScheduleResponse')[1]['lessons'])
        for lesson in lessons:
            pairs.append(Pair(
                id=lesson['id'],
                group=lesson['group']['id'],
                name=lesson['subject'],
                type=lesson['type'],
                time=PairTime(start=lesson['time']['start'], end=lesson['time']['end']),
                audiences=[x['name'] for x in lesson['audiences']],
                teachers=[x['name'] for x in lesson['teachers']],
                note=lesson['note'],
                subgroups=lesson['subgroups'],
                private=lesson['private'],
                skip=lesson['skip']
            ))
            if len(lesson['period']) == 0:
                pairs[-1].date=lesson['date'].split(',')
            else:
                period = lesson['period']
                pairs[-1].period=PairPeriod(start=period['start'], end=period['end'], weekday=period['weekday'], week=period['week'] if 'week' in period else 0)
        return pairs


    @auth_required
    def get_group_users_list(self):
        '''
        Возвращается список пользователей группы
        :return:
        '''
        return self.execute_command(GroupScheduleRequest(data={'group_id': self.user.group.id}), 'GroupUsersList')[1]

    @auth_required
    def get_editor_inital_data(self):
        '''
        Получает стартовые данные онлайн-редактора
        :return: Словарь(расписание и что-то ещё)
        '''
        return self.execute_command(EditorGroupScheduleRequest(data={'group_id': self.user.group.id}), 'EditorGroupScheduleResponse')[1]

    @auth_required
    def add_pairs(self, pairs):
        '''
        Добавляет пары
        :param pairs: Список пар
        :return: Успешность добавления
        '''
        messages = [SetLesson(data=pair.get_data()) for pair in pairs]
        return self.send_messages(messages)[0]

    @auth_required
    def delete_pairs(self, pair_ids):
        '''
        Удаляет пары с указанными id
        :param pair_ids: список id пар к удалению
        :return: Успешность удаления
        '''
        messages = [DeleteLesson(data={'id': pair_id}) for pair_id in pair_ids]
        return self.send_messages(messages)[0]

    @auth_required
    def skip_pairs(self, skip_pairs):
        '''
        Отменяет указанные пары по указанным датам
        :param skip_pairs: Список пар к отмене(PairSkip, pair_id - id пары, skip_date - дата отмены, если skip_date -
        пустая строка - отмена отмены пары)
        :return: Успешность
        '''
        messages = [SkipLesson(data={'id': pair_skip.pair_id, 'skip': pair_skip.skip_date}) for pair_skip in skip_pairs]
        return self.send_messages(messages)[0]


class Group:
    def __init__(self, id, name):
        self.id = id
        self.name = name


class User:
    def __init__(self, id, name, email, group):
        self.id = id
        self.name = name
        self.email = email
        self.group = group


PairSkip = namedtuple('PairSkip', 'pair_id skip_date')
PairTime = namedtuple('PairTime', 'start end')
PairPeriod = namedtuple('PairPeriod', 'start end weekday week')


class Pair:
    '''
     id - id пары, при создании пары указывать не нужно
     group - id группы, для которой создаётся пара
     name - название пары
     type - тип пары(Лекция и т.д.)
     time - PairTime, start - время начала пары, end - время конца(формат: 08:00)
     date - Список дат, в которые проходит пара(строка, формат: 13.02.2019)
     period - Периодичность пары, start - дата начала, end - дата конца(формат: 13.02.2019), weekday - день недели
     (0 - пн, 6 - вс), week - четность(0 - все недели, 1 - нечёт, 2 - чёт)
     Указывается либо date, либо period
     audiences - Список аудиторий
     teachers - Список преподавателей
     note - Заметка о паре
     subgroups - Строка с подгруппами
     private - Пара видна всем или только тому, кто её добавил
     skip - дата, когда пара отменена
    '''
    def __init__(self, id='', group='', name='', type='', time=None, date=None, period=None, audiences=None, teachers=None, note='', subgroups='', private=False, skip=''):
        self.id = id
        self.group = group
        self.name = name
        self.type = type
        self.time = time
        self.date = date
        self.period = period
        self.audiences = [] if audiences is None else audiences
        self.teachers = [] if teachers is None else teachers
        self.note = note
        self.subgroups = subgroups
        self.private = private
        self.skip = skip

    def get_data(self):
        data = {
            'group': self.group,
            'subject': self.name,
            'type': self.type,
            'time': {
                'start': self.time.start,
                'end': self.time.end
            },
            'audiences': [],
            'teachers': [],
            'note': self.note,
            'subgroups': self.subgroups,
            'private': self.private
        }
        for audience in self.audiences:
            data['audiences'].append({
                'name': audience,
                'addr': '',
                'dsd': '',
                'lonlat': ''
            })
        for teacher in self.teachers:
            data['teachers'].append({
                'name': teacher
            })
        if self.id!='':
            data['id'] = self.id
        if self.date is None:
            data['period'] = {
                'start': self.period.start,
                'end': self.period.end,
                'weekday': self.period.weekday,
                'week': self.period.week
            }
        else:
            data['date'] = ''
            for date in self.date:
                data['date'] += date+','
            data['date'] = data['date'][:-1]
        return data

    def __str__(self):
        return str(self.get_data())

    def __repr__(self):
        return str(self.get_data())

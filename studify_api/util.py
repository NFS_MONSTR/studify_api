import time
import uuid


def generate_uuid():
    return uuid.uuid4()


def get_current_time():
    return int(time.time())
